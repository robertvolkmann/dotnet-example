FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /src

COPY ["WeatherForecast.csproj", "./"]

RUN dotnet restore "WeatherForecast.csproj"

COPY . .

RUN dotnet publish "WeatherForecast.csproj" -c Release -o /app/publish /p:UseAppHost=false


FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /app

EXPOSE 5000

ENV ASPNETCORE_URLS=http://*:5000

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-dotnet-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

WORKDIR /app

COPY --from=build /app/publish .

ENTRYPOINT ["dotnet", "WeatherForecast.dll"]
